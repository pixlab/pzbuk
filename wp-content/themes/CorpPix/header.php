<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width,initial-scale=1.0, maximum-scale=1.0">
	<meta name="format-detection" content="telephone=no">
	<meta name="SKYPE_TOOLBAR" content ="SKYPE_TOOLBAR_PARSER_COMPATIBLE"/>

	<?php if ( is_404() ) { ?>
		<meta name="robots" content="noindex, nofollow"/>
	<?php } ?>

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> >
    
    <div id="page" class="site-page">
        
        <header id="site-header" class="site-header">
            <div class="container site-header__container">
                <a class="site-header__logo" href="<?php echo get_site_url(); ?>">
                    Comeon Group <span>players</span>
                </a>
                <?php
                if ( is_user_logged_in() ) {
                    echo '<button class="btn-primary"
                                  data-role="log-out-user">Log out</button>';
                }
                else {
	                echo '<button class="btn-primary js-open-popup-activator"
                                  data-href="#login-popup"
                                  data-role="login">Login</button>';
                }
                ?>
            </div>
        </header>
    
        <div id="site-content" class="site-content">
   