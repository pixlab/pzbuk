<?php
if ( ! function_exists( 'corppix_site_setup' ) ) :
 
	function corppix_site_setup() {
	 
		load_theme_textdomain( 'corppix_site', get_template_directory() . '/languages' );
		
		// including some required file with custom functions
		require get_template_directory() . '/includes/custom-action-filters.php';
		require get_template_directory() . '/includes/frontend-action-filters.php';
		require get_template_directory() . '/includes/helpers.php';
		require get_template_directory() . '/includes/ajax-requests.php';

	}
    add_action( 'after_setup_theme', 'corppix_site_setup' );
	
endif;


// TODO: remove after development
add_filter('show_admin_bar', '__return_false');