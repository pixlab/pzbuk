<?php

// Enable using shortcode in text widget
add_filter('widget_text', 'do_shortcode');

add_theme_support( 'post-thumbnails' );

// Allow to upload SVG images
add_filter('upload_mimes', function($mimes){
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}, 90);


// This theme uses wp_nav_menu() in two locations.
register_nav_menus( array(
	'primary'     => __( 'Primary Menu', 'corppix' ),
	'mobile_menu' => __( 'Mobile menu', 'corppix' ),
	'footer_menu' => __( 'Footer menu', 'corppix' ),
) );


// creating option page in admin area
if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> __('Theme General Settings', 'corppix'),
		'menu_title' 	=> __('Theme Settings', 'corppix'),
		'menu_slug' 	=> 'theme-general-settings',
	));
}


/**
 * Registers a "campaigns" post type
 * @uses $wp_post_types Inserts new post type object into the list
 *
 * @param string  Post type key, must not exceed 20 characters
 * @param array|string  See optional args description above.
 * @return object|WP_Error the registered post type object, or an error object
 */
function campaign_register_name() {

	$labels = array(
		'name'               => __( 'Campaigns', 'corppix' ),
		'singular_name'      => __( 'Campaign', 'corppix' ),
		'add_new'            => _x( 'Add New Campaign', 'corppix', 'corppix' ),
		'add_new_item'       => __( 'Add New Campaign', 'corppix' ),
		'edit_item'          => __( 'Edit Campaign', 'corppix' ),
		'new_item'           => __( 'New Campaign', 'corppix' ),
		'view_item'          => __( 'View Campaign', 'corppix' ),
		'search_items'       => __( 'Search Campaigns', 'corppix' ),
		'not_found'          => __( 'No Campaigns found', 'corppix' ),
		'not_found_in_trash' => __( 'No Campaigns found in Trash', 'corppix' ),
		'parent_item_colon'  => __( 'Parent Campaign:', 'corppix' ),
		'menu_name'          => __( 'Campaigns', 'corppix' ),
	);

	$args = array(
		'labels'              => $labels,
		'hierarchical'        => false,
		'description'         => 'description',
		'taxonomies'          => array(),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => null,
		'menu_icon'           => null,
		'show_in_nav_menus'   => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,
		'rewrite'             => true,
		'capability_type'     => 'post',
		'supports'            => array(
			'title',
			'editor',
			'author',
			'thumbnail',
			'excerpt',
			'custom-fields',
			'trackbacks',
			'comments',
			'revisions',
			'page-attributes',
			'post-formats',
		),
	);

	register_post_type( 'campaigns', $args );
}

add_action( 'init', 'campaign_register_name' );



add_action('wp_footer', function(){

	if ( !is_user_logged_in() ) {
		include( locate_template( 'includes/custom-popups.php',
			false,
			false ) );
	}

}, 90);