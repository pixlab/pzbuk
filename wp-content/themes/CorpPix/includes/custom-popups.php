<div id="login-popup" class="popup">
	<div class="popup__overlay js-popup-close"></div>

	<div class="popup__inner-wrapper">
		<div class="popup__inner">

			<p class="popup__caption"><?php _e('Enter your login details'); ?></p>

			<form method="post" class="form__wrapper" data-role="force-login">
				<div class="form__input-holder">
					<label for="user_email" class="form__label">
						<?php _e('Your email'); ?>
					</label>
					<input type="text" id="user_email" name="user_email" class="form__input" />
					<div class="form__input-error"></div>
				</div>
				<div class="form__input-holder">
					<label for="user_password" class="form__label">
						<?php _e('Your password'); ?>
					</label>
					<input type="password" id="user_password" name="user_password" class="form__input" />
				</div>
				<div class="form__input-holder">
					<input type="submit" class="form__submit js-form-submit" value="Send" />
				</div>


                <a class="form__reset-link"
                   href="<?php echo get_site_url() . '/wp-login.php?action=lostpassword'; ?>">
                    Reset Your password
                </a>

                <div class="form__status js-form-status"></div>
			</form>
		</div>

		<button class="popup__close js-popup-close"></button>
	</div>
</div>