<?php
/**
 * Enqueue scripts and styles
 */
function corppix_site_scripts() {
	
	// Load our main stylesheet.
	wp_enqueue_style( 'corppix_site-style', get_stylesheet_uri() );
	
	// General font family
	wp_enqueue_style('Libre_franklin_fonts', 'https://fonts.googleapis.com/css2?family=Libre+Franklin:wght@300;400;700&display=swap" rel="stylesheet');
	
	// Custom generated styles
	wp_enqueue_style('corppix_site_new_style', get_template_directory_uri().'/build/styles/screen.css');
	
	// General javascript libraries
	wp_enqueue_script( 'libs_js', get_template_directory_uri() . '/build/js/libs.js', array('jquery'), null, true );
	
	// Customization javascript
	wp_enqueue_script( 'customization_js', get_template_directory_uri() . '/build/js/customization.js', array('jquery'), null, true );
	
	// Sending some data to frontend
	$vars = array(
		'ajax_url'   => admin_url( 'admin-ajax.php' ),
		'theme_path' => get_stylesheet_directory_uri(),
		'site_url'   => get_site_url()
	);
	wp_localize_script( 'customization_js', 'var_from_php', $vars );
}

add_action( 'wp_enqueue_scripts', 'corppix_site_scripts' );


/**
 * Remove all scripts from head section
 * and move them to footer
 */
function custom_clean_head() {
	remove_action('wp_head', 'wp_print_scripts');
	remove_action('wp_head', 'wp_print_head_scripts', 9);
	remove_action('wp_head', 'wp_enqueue_scripts', 1);
	
	add_action('wp_footer', 'wp_print_scripts', 5);
	add_action('wp_footer', 'wp_enqueue_scripts', 5);
	add_action('wp_footer', 'wp_print_head_scripts', 5);
}

add_action( 'wp_enqueue_scripts', 'custom_clean_head' );