<?php
/**
 * Reset user password
 */
function reset_customer_password() {
	
	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) { // check if it's ajax request (simple defence)
		
		global $global_options;
		
		//We shall SQL escape all inputs
		$user_email = filter_input(INPUT_POST, 'recovery-email', FILTER_SANITIZE_STRING);
		
		// get user meta data
		$userMeta = null;
		
		if ( filter_var($user_email, FILTER_VALIDATE_EMAIL) ) {
			$userMeta = get_user_by( 'email', $user_email );
		} else {
			$userMeta = get_user_by( 'login', $user_email );
		}
		
		if ( $userMeta ) {
			$userId = $userMeta->ID;
			$user      = get_userdata($userId );
			$userLogin = $user->user_login;
			$user_email = $user->user_email;
			$rand_key = md5( uniqid( rand(), true ) );
			set_transient( 'temp_key_'.$userId, $rand_key, 60*15 );
			
			// get user activation key
			$user_activation_hash = get_password_reset_key( $user );
			
			$msg = __( 'Привет!', 'personalize-login' ) . "\r\n\r\n";
			$msg .= sprintf( __( 'Вы попросили нас сбросить пароль для вашей учетной записи, используя адрес электронной почты %s.', 'personalize-login' ), $user_email ) . "\r\n\r\n";
			$msg .= __( "Если это была ошибка или вы не просили сбросить пароль, просто проигнорируйте это письмо, и ничего не произойдет.", 'personalize-login' ) . "\r\n\r\n";
			$msg .= __( 'Чтобы сбросить пароль, посетите следующий адрес:', 'personalize-login' ) . "\r\n\r\n";
			$msg .= site_url( "wp-login.php?action=rp&key=$user_activation_hash&login=" . rawurlencode( $userLogin ), 'login' ) . "\r\n\r\n";
			$msg .= 'Ваш проверочный код: '.$userId.'1der1'.$rand_key;
			$msg .= "\r\n\r\n";
			$msg .= __( 'Спасибо!', 'personalize-login' ) . "\r\n";
			$msg .= "-------------------\r\n";
			$msg .= site_url() . "\r\n\r\n";
			
			$headers = array( 'Content-Type: text; charset=UTF-8' );
			
			$subject = $global_options['translation_string_group']['new_password'];
			
			$response_text_success = pll__('Пожалуйста, проверьте свой электронный ящик');
			
			$response_text_error = pll__('Что-то пошло не так ... Повторите попытку позже.');
			
			if ( wp_mail( $user_email, $subject, $msg, $headers ) ) {
				wp_send_json_success( $response_text_success );
			} else {
				wp_send_json_success( $response_text_error );
			}
			
		} else {
			wp_send_json_success( pll__('Указанные данные не связаны ни с одним пользователем.') );
		}
	}
	
	die();
	
}
add_action( 'wp_ajax_nopriv_reset_customer_password', 'reset_customer_password' );
add_action( 'wp_ajax_reset_customer_password', 'reset_customer_password' );



/**
 * Check and login user
 */
function check_and_login_user(){
	if ( defined( 'DOING_AJAX' ) && DOING_AJAX ) { // check if it's ajax request (simple defence)
		$email = filter_input( INPUT_POST, 'user_email', FILTER_SANITIZE_EMAIL );
		$password    = filter_input( INPUT_POST, 'user_password', FILTER_SANITIZE_STRING );
		
		$login_data = array();
		
		//We shall SQL escape all inputs
		$login_data['user_login']    = $email;
		$login_data['user_password'] = $password;
		
		if ( $login_data['user_login'] && $login_data['user_password'] ) {

			// Get user segment from https://promocje.pzbuk.pl API
			$user_status_json = getPzbukStatus($email);
			$user_status      = json_decode($user_status_json);

			if ( $user_status->status === 'ERROR' ) {
				wp_send_json_error( $user_status->message);
			}

			// Get user segment from response
			$user_segment = $user_status->result->value;

			// force login user
			$user_verify = wp_signon( $login_data, false );

			if ( is_wp_error( $user_verify ) ) {
				wp_send_json_error( 'Invalid login details');
			}

			update_field('user_segment', $user_segment, 'user_'.$user_verify->ID);

			wp_send_json_success();
		}
		
		wp_send_json_error( 'Something goes wrong...');
	}
	die;
}
add_action( 'wp_ajax_nopriv_check_and_login_user', 'check_and_login_user' );



/**
 * Check and register user
 */

function check_and_register_user(){
	$mail = filter_input( INPUT_POST, 'register-email', FILTER_SANITIZE_STRING );
	
	if ( filter_var($mail, FILTER_VALIDATE_EMAIL) ) {
		$userMeta = get_user_by( 'email', $mail );
		
		if($userMeta) {
			wp_send_json_success( pll__('Пользователь с таким e-mail уже существует') );
		} else {
			
			$username = explode('@', $mail)[0];
			$rand_password = uniqid( rand(), true );
			$user_id = wp_create_user( $username, $rand_password, $mail );
			
			if ( !is_wp_error($user_id) ) {
				
				$headers = array( 'Content-Type: text; charset=UTF-8' );
				$subject = 'Регистрация';
				$msg = pll__( 'Привет!') . "\r\n\r\n";
				$msg .= pll__('Вы зарегистрировались на сайте ').' '. get_home_url() . "\r\n\r\n";
				$msg .= pll__('Ваш Login:').' '. $username. "\r\n\r\n";
				$msg .= pll__('Ваш Password: ').' '. $rand_password. "\r\n\r\n";
				$msg .= pll__('Спасибо что вы с нами!'). "\r\n\r\n";
				$msg .= "-------------------\r\n";
				$msg .= site_url() . "\r\n\r\n";
				
				if ( wp_mail( $mail, $subject, $msg, $headers ) ) {
					echo json_encode( array('success'=>true,
					                        'data'=> "",
					                        'new_user' => pll__('Письмо с логином и паролем для входа отправленно вам на почту.')));
				} else {
					wp_send_json_success( pll__('Что-то пошло не так ... Повторите попытку позже.') );
				}
			}
		}
		
	} else {
		wp_send_json_success( pll__('Введите корректный e-mail') );
	}
	
	die;
}
add_action( 'wp_ajax_nopriv_check_and_register_user', 'check_and_register_user' );



/**
 * Log out user
 */

function log_out_user(){
	wp_logout();
	wp_send_json_success('Logout');
	die;
}
add_action( 'wp_ajax_log_out_user', 'log_out_user' );