<?php
/**
 * The template for displaying single campaign
 *
 */
get_header();
?>

<div class="page-content__main-wrapper">

	<?php
	do_action('pixlab_before_page_content');

	if ( have_posts() ) :
		// Start the loop.
		while ( have_posts() ) : the_post();

			$segment_content = get_field('campaign_segment_content');
			$user_segment    = get_field('user_segment', 'user_'.get_current_user_id() );

			//var_dump('$segment_content', $segment_content);

			if ( !empty($segment_content) && is_array($segment_content) ) {
				foreach ( $segment_content as $item ) {

					if ( $item['next_content_for'] !== $user_segment ) continue;
					?>
					<div class="campaign-content__wrapper">
                        <div class="container page-content_container">
                            <div class="campaign-content__header">
                                <?php echo do_shortcode( $item['header_text'] ); ?>
                            </div>

                            <div class="campaign-content__description">
                                <?php echo do_shortcode( $item['description_text'] ); ?>
                            </div>

                            <a href="<?php echo $item['button-cta_url']; ?>"
                               class="campaign-content__cta-btn">
                                <?php echo $item['button-cta_title']; ?>
                            </a>

                            <div class="campaign-content__disclaimer">
                                <?php echo do_shortcode( $item['disclaimer_text​'] ); ?>
                            </div>
                        </div>
					</div>
					<?php
				}
			}

		endwhile;
	endif;

	do_action('pixlab_after_page_content');
	?>
</div>

<?php get_footer(); ?>