import smoothscroll from 'smoothscroll-polyfill';

// kick off the polyfill!
smoothscroll.polyfill();

/**
 * Fade Out method
 * @param el
 */
export function fadeOut( el ) {

	if ( !el ) {
		throw Error( '"fadeOut function - "You didn\'t add required parameters' );
	}

	el.style.opacity = 1;

	(function fade() {
		if ( (el.style.opacity -= .1) < 0 ) {
			el.style.display = "none";
		} else {
			requestAnimationFrame( fade );
		}
	})();
}


/**
 * Fade In method
 * @param el      - element that need to fade in
 * @param display - display variant
 */
export function fadeIn( el, display ) {

	if ( !el ) {
		throw Error( '"fadeIn function - "You didn\'t add required parameters' );
	}

	el.style.opacity = 0;
	el.style.display = display || "block";

	(function fade() {
		let val = parseFloat( el.style.opacity );
		if ( !((val += .1) > 1) ) {
			el.style.opacity = val;
			requestAnimationFrame( fade );
		}
	})();
}


/**
 *  Set equal height to selected elements calculated as bigger height
 * @param elementsSelector  - selector for searching elements
 * @param minOrMax          - Define what dimension should be calculated (minHeight or maxHeight)
 * @returns elementsSelector
 */
export function equalHeights( elementsSelector, minOrMax = 'max' ) {

	if ( !elementsSelector ) {
		throw Error( '"equalHeights function - "You didn\'t add required parameters' );
	}

	let heights = [];
	let elementsSelectorArr = (Array.isArray( elementsSelector ))
		? elementsSelector
		: [...document.querySelectorAll( elementsSelector )];

	elementsSelectorArr.forEach( ( item ) => {
		item.style.height = 'auto';
	} );

	elementsSelectorArr.forEach( ( item ) => {
		heights.push( item.offsetHeight );
	} );

	let commonHeight = (minOrMax === 'max')
		? Math.max.apply( 0, heights )
		: Math.min.apply( 0, heights );

	elementsSelectorArr.forEach( ( item ) => {
		item.style.height = commonHeight + 'px';
	} );

	return elementsSelector;

}


/**
 * Set equal height to selected elements in row calculated as bigger height
 * @param elementsSelector - selector for searching elements
 * @param numItem_inrow    - Items amount that will be used for each equal height iteration
 * @returns elementsSelector
 */
export function equalHeights_inrow( elementsSelector, numItem_inrow ) {

	if ( !elementsSelector || !numItem_inrow ) {
		throw Error( '"equalHeights_inrow function - "You didn\'t add required parameters' );
	}

	const ELEMENTS_ARR = [...document.querySelectorAll( elementsSelector )];
	const EL_LENGTH = ELEMENTS_ARR.length;

	for ( let i = 0; i <= EL_LENGTH / numItem_inrow; i++ ) {
		let temp = ELEMENTS_ARR.slice( i * numItem_inrow, i * numItem_inrow + numItem_inrow );
		equalHeights( temp );
	}

	return elementsSelector;
}


/**
 * Trim all paragraph from unneeded space symbol
 */
export function trimParagraph() {
	[...document.querySelectorAll( 'p' )].forEach( item => {
		item.innerHTML = item.innerHTML.trim();
	} );
}


/**
 * Check if element in viewport
 * @param el
 * @param offset - Adjustable offset value when element becomes visible
 * @returns {boolean}
 */
export function isInViewport( el, offset = 100 ) {

	if ( !el ) {
		throw Error( '"isInViewport function - "You didn\'t add required parameters' );
	}

	const scroll = window.scrollY || window.pageYOffset;
	const boundsTop = el.getBoundingClientRect().top + offset + scroll;

	const viewport = {
		top: scroll,
		bottom: scroll + window.innerHeight,
	};

	const bounds = {
		top: boundsTop,
		bottom: boundsTop + el.clientHeight,
	};

	return (bounds.bottom >= viewport.top && bounds.bottom <= viewport.bottom)
		|| (bounds.top <= viewport.bottom && bounds.top >= viewport.top);

}


/**
 * Debounce function
 * @param fn     - function that should be executed
 * @param time   - time delay
 * @returns {Function}
 */
export const debounce = ( fn, time = 1000 ) => {

	if ( !fn ) {
		throw Error( '"debounce function - "You didn\'t add required parameters' );
	}

	let timeout;

	return function () {
		const functionCall = () => fn.apply( this, arguments );

		clearTimeout( timeout );
		timeout = setTimeout( functionCall, time );
	}
};


/**
 * Copy to clipboard
 * @param element -  element that  contain value to copy
 */
export const copyToClipboard = ( parent, element ) => {

	if ( !parent || !element ) {
		throw Error( '"copyToClipboard function - "You didn\'t add required parameters' );
	}

	const el = document.createElement( 'textarea' );
	el.value = element.value;
	document.body.appendChild( el );
	el.select();

	try {
		const successful = document.execCommand( 'copy' );

		if ( successful ) {
			parent.classList.add( 'copied' );

			setTimeout( () => {
				parent.classList.remove( 'copied' );
			}, 3000 );
		}
	} catch ( err ) {
		console.log( 'Oops, unable to copy' );
	}

	document.body.removeChild( el );
};


/**
 * Test value with regex
 * @param {(name|email|phone|postal)} fieldType  - The allowed type of the fields
 * @param value
 * @return {boolean}
 */
export const validateField = ( fieldType = null, value = null ) => {

	if ( !fieldType || !value ) {
		throw Error( '"validateField function - "You didn\'t add required parameters' );
	}

	const phoneREGEX = /^[0-9\+]{6,13}$/;
	const nameREGEX = /^[a-zA-Zа-яА-Я\s]{2,30}$/;
	const postalREGEX = /^[A-Z]{1,2}[0-9]{1,2} ?[0-9][A-Z]{2}$/i;
	const emailREGEX = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	const dummyREGEX = /^.+$/;

	let checkResult = false;

	switch ( fieldType ) {
		case 'name':
			checkResult = nameREGEX.test( value );
			break;
		case 'phone':
			checkResult = phoneREGEX.test( value );
			break;
		case 'postal':
			checkResult = postalREGEX.test( value );
			break;
		case 'email':
			checkResult = emailREGEX.test( value );
			break;
		case 'price':
			checkResult = dummyREGEX.test( value );
			break;
		case 'aim':
			checkResult = dummyREGEX.test( value );
			break;
		case 'date':
			checkResult = dummyREGEX.test( value );
			break;
		case 'subject':
			checkResult = dummyREGEX.test( value );
			break;
	}

	return checkResult;

};


/**
 * Polyfill for closest method
 */
export function closest_polyfill() {
	if ( window.Element && !Element.prototype.closest ) {
		Element.prototype.closest =
			function ( s ) {
				let matches = (this.document || this.ownerDocument).querySelectorAll( s ),
					i,
					el = this;
				do {
					i = matches.length;
					while ( --i >= 0 && matches.item( i ) !== el ) {
					}
					;
				} while ( (i < 0) && (el = el.parentElement) );
				return el;
			};
	}
}


/**
 * Smooth scroll to element on page
 * @param elementsSelector string -- css selector (anchor links)
 * @param callback function       -- callback for some additional actions
 */
export function anchorLinkScroll( elementsSelector, callback = '' ) {

	if ( !elementsSelector ) {
		throw Error( '"anchorLinkScroll function - "You didn\'t add correct selector for anchor links' );
	}

	const elements = document.querySelectorAll( elementsSelector );

	(elements) && [...elements].forEach( link => {

		link.addEventListener( 'click', ( event ) => {
			event.preventDefault();

			let el_href = (event.target.nodeName === 'A')
				? event.target.getAttribute( 'href' )
				: event.target.dataset.href;

			const ANCHOR_ELEMENT = document.querySelector( el_href );

			(ANCHOR_ELEMENT) && window.scroll( {
				'behavior': 'smooth',
				'left': 0,
				'top': ANCHOR_ELEMENT.offsetTop
			} );

			if ( callback ) callback();

		} );

	} );

}



/**
 * Check and login user functionality
 * @param form_instance
 */
export const check_and_login_user = async ( form_instance ) => {

	if ( !form_instance ) {
		throw Error( '"check_and_login_user" function - You didn\'t add required parameters' );
	}

	const EMAIL_INPUT          = form_instance['user_email'];
	const EMAIL_INPUT_ERROR_EL = EMAIL_INPUT.nextElementSibling;
	const SUBMIT_BTN_EL        = form_instance.querySelector('.js-form-submit');
	const FORM_STATUS_EL       = form_instance.querySelector('.js-form-status');

	const VALIDATE_AS_EMAIL = validateField( 'email', EMAIL_INPUT.value );

	// Show error message when user entered invalid email
	if ( !VALIDATE_AS_EMAIL && EMAIL_INPUT_ERROR_EL ) {
		EMAIL_INPUT_ERROR_EL.innerHTML = 'Please enter valid email';
		return;
	}

	(SUBMIT_BTN_EL) && SUBMIT_BTN_EL.classList.add( 'loading' );

	if ( EMAIL_INPUT_ERROR_EL ) {
		EMAIL_INPUT_ERROR_EL.innerHTML = '';
	}

	const formData = new FormData( form_instance );
	formData.append( 'action', 'check_and_login_user' );

	fetch( var_from_php.ajax_url,
		{
			method: 'POST',
			body: formData,
		} )
		.then( response => response.json() )
		.then( response => {
			(SUBMIT_BTN_EL) && SUBMIT_BTN_EL.classList.remove( 'loading' );

			if ( response.success ) {
				window.location.reload();
			}

			if (FORM_STATUS_EL) {
				FORM_STATUS_EL.innerHTML = response.data;
			}

		} );

}



/**
 * Force logout user functionality
 */
export const force_logout = async () => {

	const formData = new FormData(  );
	formData.append( 'action', 'log_out_user' );

	fetch( var_from_php.ajax_url,
		{
			method: 'POST',
			body: formData,
		} )
		.then( response => response.json() )
		.then( response => {
			window.location.reload();
		} );

}