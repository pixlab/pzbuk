import smoothscroll from 'smoothscroll-polyfill';
import Popup        from './modules/popup-window.js';

import {
	closest_polyfill,
	check_and_login_user,
	force_logout,
} from './modules/helpers.js';

(function ($) {

window.addEventListener('DOMContentLoaded', (event) => {

	// kick off the polyfill!
	smoothscroll.polyfill();

	// Init Closest polyfill method
	closest_polyfill();

	// Init Popup Windows
	const popup_instance = new Popup();
	popup_instance.init();


	/**
	 * Global "click" Handler
	 */
	document.body.addEventListener( 'click', event => {
		const ROLE = event.target.dataset.role;

		if ( !ROLE ) return;

		switch ( ROLE ) {

			case 'log-out-user':
				event.preventDefault();
				( async () => {
					force_logout();
				})();
				break;

		}
	});


	/**
	 * Global "submit" Handler
	 */
	document.body.addEventListener( 'submit', event => {
		const ROLE = event.target.dataset.role;

		if ( !ROLE ) return;

		switch ( ROLE ) {

			// Login to CMS
			case 'force-login':
				event.preventDefault();
				( async () => {
					check_and_login_user(event.target);
				})();
				break;

		}
	});

});  // end of "DOMContentLoaded"

})(jQuery);




