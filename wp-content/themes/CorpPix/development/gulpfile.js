const gulp = require('gulp');

const { src, dest, watch, series, parallel } = require('gulp');
const sass       = require('gulp-sass');
const sourcemaps = require('gulp-sourcemaps');
const uglify     = require('gulp-uglify');
const concat     = require('gulp-concat');
const gutil      = require('gulp-util');
const browserify = require('gulp-browserify');

sass.compiler = require('node-sass');

const paths = {
    styles: {
        src: 'sass/**/*.scss',
        dest: '../build/styles/'
    },
    scripts: {
        src: 'js/**/*.js',
        dest: '../build/js/'
    }
};


function js_compile(){
	return gulp.src('js/customization.js')
		.pipe(browserify({
			transform: ['babelify'],
		}))
		.pipe(uglify())
		.pipe(gulp.dest(paths.scripts.dest))
		.on('error', gutil.log);
}


function combile_libs_js(){
	return gulp.src([
			'js/slick.js',
		])
		.pipe(concat('libs.js'))
		.pipe(gulp.dest(paths.scripts.dest))
		.on('error', gutil.log);
}



function styles() {
    return gulp.src(paths.styles.src)
        .pipe(sourcemaps.init())
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest(paths.styles.dest));
}


function watch2() {
    gulp.watch(paths.styles.src, styles);
    gulp.watch(paths.scripts.src, combile_libs_js);
    gulp.watch(paths.scripts.src, js_compile);
}

function build() {
	styles();
	combile_libs_js();
	js_compile();
}

/*
 * You can use CommonJS `exports` module notation to declare tasks
 */
exports.watch = watch2;
exports.build = gulp.series(styles, gulp.parallel(combile_libs_js, js_compile));



