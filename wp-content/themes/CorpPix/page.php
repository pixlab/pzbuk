<?php
/**
 * The template for displaying pages
 *
 */
get_header();
?>

<div class="page-content__main-wrapper">
    <div class="container page-content_container">
        <?php
        do_action('pixlab_before_page_content');

        if ( have_posts() ) :
            // Start the loop.
            while ( have_posts() ) : the_post();
                the_content();
            endwhile;
        endif;

        do_action('pixlab_after_page_content');
        ?>
    </div>
</div>

<?php get_footer(); ?>